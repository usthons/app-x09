package usthon.syafii.app092c

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context) : SQLiteOpenHelper(context, DB_Name,null, DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val Mp = "create table mp(id_music text, id_cover text, music_title text)"
        val insMp = "insert into mp values('0x7f0c0000', '0x7f06005f', 'Hym For The Weekend'), ('0x7f0c0001', '0x7f060060', 'Girls Like You'), ('0x7f0c0002', '0x7f060061', 'Moves Like Jagger')"
        db?.execSQL(Mp)
        db?.execSQL(insMp)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "app09"
        val DB_Ver = 1
    }

}