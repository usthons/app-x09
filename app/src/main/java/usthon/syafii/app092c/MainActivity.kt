package usthon.syafii.app092c

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    lateinit var db : SQLiteDatabase

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                audioPlay(idMusik,idCover,idJudul)
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }

    val daftarLagu = intArrayOf(R.raw.music1, R.raw.music2, R.raw.music3)
    val daftarCover = intArrayOf(R.drawable.cover1, R.drawable.cover2, R.drawable.cover3)
    val daftarJudul = arrayOf("Hym For The Weekend", "Girls Like You", "Moves Like Jagger")

    var posLaguSkrg = 0
    var posVidSkrg = 0
    var posCoverSkrg = 0
    var posJudulSkrg = ""
    var handler = Handler()

    lateinit var mediaPlayer : MediaPlayer
    lateinit var mediaController: MediaController
    lateinit var adapter : ListAdapter
    lateinit var  v : View

    var idMusik : Int = 0x7f0c0002
    var idCover : Int = 0x7f060061
    var idJudul : String = "Moves Like Jagger"
    var playM : Int = idMusik
    var playC : Int = idCover
    var playJ : String = idJudul
    var max : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DBOpenHelper(this).writableDatabase
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max=100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        lsMp.setOnItemClickListener(itemClicked)
    }

    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        val menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(a:Int,b:Int,c:String){
        mediaPlayer.stop()
        mediaPlayer = MediaPlayer.create(this,a)
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(b)
        txJudulLagu.setText(idJudul)
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun play(){
        posLaguSkrg = playM
        posCoverSkrg = playC
        posJudulSkrg = playJ
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
        if(posLaguSkrg < 0x7f0c0002){
            posLaguSkrg++
            posCoverSkrg++
            jud(posLaguSkrg)
        } else {
            posLaguSkrg = 0x7f0c0000
            posCoverSkrg = 0x7f06005f
            jud(posLaguSkrg)
        }
        audioPlay(posLaguSkrg,posCoverSkrg,posJudulSkrg)
    }

    fun jud(a:Int){
        if(a==0x7f0c0000){
            idJudul="Hym For The Weekend"

        }
        else if(a==0x7f0c0001){
            idJudul="Girls Like You"
        }
        else{
            idJudul="Moves Like Jagger"
        }
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
        if(posLaguSkrg > 0x7f0c0000){
            posLaguSkrg--
            posCoverSkrg--
            jud(posLaguSkrg)
        } else {
            posLaguSkrg = 0x7f0c0002
            posCoverSkrg = 0x7f060061
            jud(posLaguSkrg)
        }
        audioPlay(posLaguSkrg,posCoverSkrg,posJudulSkrg)
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    fun showDataMp(){
        var sql = "select id_music as _id, id_cover as cover, music_title as judul from mp"
        max = "select count(id_music) as i from mp"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this,R.layout.list_musik,c, arrayOf("_id","cover","judul"),
            intArrayOf(R.id.txtIdMusik,R.id.txtIdCover,R.id.txtMusikTitle),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMp.adapter = adapter
    }

    val itemClicked = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idMusik = c.getInt(c.getColumnIndex("_id"))
        txJudulLagu.setText(c.getString(c.getColumnIndex("judul")))
        idCover = c.getInt(c.getColumnIndex("cover"))
        idJudul = c.getString(c.getColumnIndex("judul"))
        audioPlay(idMusik,idCover,idJudul)
    }

    fun getDBObject():SQLiteDatabase{
        return db
    }

    override fun onStart() {
        super.onStart()
        showDataMp()
        play()
    }

    inner class UpdateSeekBarProgressThread : Runnable {
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress = currTime
            if(currTime != mediaPlayer.duration){
                handler.postDelayed(this, 50)
            }
        }
    }

}
